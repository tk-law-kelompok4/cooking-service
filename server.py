import asyncio
import logging
import grpc
import jwt

from log4mongo.handlers import BufferedMongoHandler

from api.service.service import cook

import cooking_pb2
import cooking_pb2_grpc
import orderservice_pb2
import orderservice_pb2_grpc
import delivery_pb2
import delivery_pb2_grpc

# Coroutines to be invoked when the event loop is shutting down.
_cleanup_coroutines = []

COOKING_GRPC_PORT = 50060
MONGODB_URL = "mongodb+srv://PIZZAKU:Hthg027n2uTubGZa@cluster0.rrcy1.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"

# Setup logging
logging.basicConfig(level=logging.INFO)
handler = BufferedMongoHandler(
    host=MONGODB_URL, database_name="logging-service", collection="logs"
)
logger = logging.getLogger("cooking-server")
logger.addHandler(handler)
logger.info("starting grpc server")


class Cooking(cooking_pb2_grpc.CookingServicer):
    async def Cook(self, request, context):
        # JWT
        payload = jwt.decode(request.accessToken, options={"verify_signature": False})

        # Logging
        logger.info(
            f'Cook request received, request: {payload["role"]} - {request.orderId} - {type(request)}'
        )

        if payload["role"] != "chef":
            logger.info("Role invalid")
            return cooking_pb2.CookReply(status=403, message="role is not chef")
        res = cook(request.items)
        logger.info(
            f'Changing orderId {request.orderId} status from "COOKING" to "SENDING"'
        )
        with grpc.insecure_channel("localhost:50053") as channel:
            stub = orderservice_pb2_grpc.OrderServiceStub(channel)
            response = stub.UpdateOrder(
                orderservice_pb2.UpdateOrderRequest(
                    accessToken=request.accessToken,
                    orderId=request.orderId,
                    orderStatus="sending",
                )
            )
        logger.info(
            f'Request delivery by orderId {request.orderId}'
        )
        with grpc.insecure_channel("localhost:50070") as channel:
            stub = delivery_pb2_grpc.DeliveryStub(channel)
            response = stub.Deliver(
                accessToken=request.accessToken,
                orderId=request.orderId
            )
        return cooking_pb2.CookReply(status=res["status"], message=res["message"])


async def serve():
    server = grpc.aio.server()
    cooking_pb2_grpc.add_CookingServicer_to_server(Cooking(), server)
    server.add_insecure_port(f"[::]:{COOKING_GRPC_PORT}")
    await server.start()

    async def server_graceful_shutdown():
        logging.info("Starting graceful shutdown...")
        # Shuts down the server with 0 seconds of grace period. During the
        # grace period, the server won't accept new connections and allow
        # existing RPCs to continue within the grace period.
        await server.stop(5)

    _cleanup_coroutines.append(server_graceful_shutdown())
    await server.wait_for_termination()


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(serve())
    finally:
        loop.run_until_complete(*_cleanup_coroutines)
        loop.close()
