import time


async def cook(items: list[dict[str, str]]):
    time.sleep(len(items) * 30)
    return {"status": 200, "message": "Cooking finish, sending now"}
