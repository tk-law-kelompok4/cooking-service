# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
"""Client and server classes corresponding to protobuf-defined services."""
import grpc

import orderservice_pb2 as orderservice__pb2


class OrderServiceStub(object):
    """Missing associated documentation comment in .proto file."""

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.Menu = channel.unary_unary(
            "/orderservice.OrderService/Menu",
            request_serializer=orderservice__pb2.MenuRequest.SerializeToString,
            response_deserializer=orderservice__pb2.MenuReply.FromString,
        )
        self.CreateOrder = channel.unary_unary(
            "/orderservice.OrderService/CreateOrder",
            request_serializer=orderservice__pb2.CreateOrderRequest.SerializeToString,
            response_deserializer=orderservice__pb2.CreateOrderReply.FromString,
        )
        self.GetOrders = channel.unary_unary(
            "/orderservice.OrderService/GetOrders",
            request_serializer=orderservice__pb2.GetOrdersRequest.SerializeToString,
            response_deserializer=orderservice__pb2.GetOrdersReply.FromString,
        )
        self.GetOrder = channel.unary_unary(
            "/orderservice.OrderService/GetOrder",
            request_serializer=orderservice__pb2.GetOrderRequest.SerializeToString,
            response_deserializer=orderservice__pb2.GetOrderReply.FromString,
        )
        self.UpdateOrder = channel.unary_unary(
            "/orderservice.OrderService/UpdateOrder",
            request_serializer=orderservice__pb2.UpdateOrderRequest.SerializeToString,
            response_deserializer=orderservice__pb2.UpdateOrderReply.FromString,
        )


class OrderServiceServicer(object):
    """Missing associated documentation comment in .proto file."""

    def Menu(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details("Method not implemented!")
        raise NotImplementedError("Method not implemented!")

    def CreateOrder(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details("Method not implemented!")
        raise NotImplementedError("Method not implemented!")

    def GetOrders(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details("Method not implemented!")
        raise NotImplementedError("Method not implemented!")

    def GetOrder(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details("Method not implemented!")
        raise NotImplementedError("Method not implemented!")

    def UpdateOrder(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details("Method not implemented!")
        raise NotImplementedError("Method not implemented!")


def add_OrderServiceServicer_to_server(servicer, server):
    rpc_method_handlers = {
        "Menu": grpc.unary_unary_rpc_method_handler(
            servicer.Menu,
            request_deserializer=orderservice__pb2.MenuRequest.FromString,
            response_serializer=orderservice__pb2.MenuReply.SerializeToString,
        ),
        "CreateOrder": grpc.unary_unary_rpc_method_handler(
            servicer.CreateOrder,
            request_deserializer=orderservice__pb2.CreateOrderRequest.FromString,
            response_serializer=orderservice__pb2.CreateOrderReply.SerializeToString,
        ),
        "GetOrders": grpc.unary_unary_rpc_method_handler(
            servicer.GetOrders,
            request_deserializer=orderservice__pb2.GetOrdersRequest.FromString,
            response_serializer=orderservice__pb2.GetOrdersReply.SerializeToString,
        ),
        "GetOrder": grpc.unary_unary_rpc_method_handler(
            servicer.GetOrder,
            request_deserializer=orderservice__pb2.GetOrderRequest.FromString,
            response_serializer=orderservice__pb2.GetOrderReply.SerializeToString,
        ),
        "UpdateOrder": grpc.unary_unary_rpc_method_handler(
            servicer.UpdateOrder,
            request_deserializer=orderservice__pb2.UpdateOrderRequest.FromString,
            response_serializer=orderservice__pb2.UpdateOrderReply.SerializeToString,
        ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
        "orderservice.OrderService", rpc_method_handlers
    )
    server.add_generic_rpc_handlers((generic_handler,))


# This class is part of an EXPERIMENTAL API.
class OrderService(object):
    """Missing associated documentation comment in .proto file."""

    @staticmethod
    def Menu(
        request,
        target,
        options=(),
        channel_credentials=None,
        call_credentials=None,
        insecure=False,
        compression=None,
        wait_for_ready=None,
        timeout=None,
        metadata=None,
    ):
        return grpc.experimental.unary_unary(
            request,
            target,
            "/orderservice.OrderService/Menu",
            orderservice__pb2.MenuRequest.SerializeToString,
            orderservice__pb2.MenuReply.FromString,
            options,
            channel_credentials,
            insecure,
            call_credentials,
            compression,
            wait_for_ready,
            timeout,
            metadata,
        )

    @staticmethod
    def CreateOrder(
        request,
        target,
        options=(),
        channel_credentials=None,
        call_credentials=None,
        insecure=False,
        compression=None,
        wait_for_ready=None,
        timeout=None,
        metadata=None,
    ):
        return grpc.experimental.unary_unary(
            request,
            target,
            "/orderservice.OrderService/CreateOrder",
            orderservice__pb2.CreateOrderRequest.SerializeToString,
            orderservice__pb2.CreateOrderReply.FromString,
            options,
            channel_credentials,
            insecure,
            call_credentials,
            compression,
            wait_for_ready,
            timeout,
            metadata,
        )

    @staticmethod
    def GetOrders(
        request,
        target,
        options=(),
        channel_credentials=None,
        call_credentials=None,
        insecure=False,
        compression=None,
        wait_for_ready=None,
        timeout=None,
        metadata=None,
    ):
        return grpc.experimental.unary_unary(
            request,
            target,
            "/orderservice.OrderService/GetOrders",
            orderservice__pb2.GetOrdersRequest.SerializeToString,
            orderservice__pb2.GetOrdersReply.FromString,
            options,
            channel_credentials,
            insecure,
            call_credentials,
            compression,
            wait_for_ready,
            timeout,
            metadata,
        )

    @staticmethod
    def GetOrder(
        request,
        target,
        options=(),
        channel_credentials=None,
        call_credentials=None,
        insecure=False,
        compression=None,
        wait_for_ready=None,
        timeout=None,
        metadata=None,
    ):
        return grpc.experimental.unary_unary(
            request,
            target,
            "/orderservice.OrderService/GetOrder",
            orderservice__pb2.GetOrderRequest.SerializeToString,
            orderservice__pb2.GetOrderReply.FromString,
            options,
            channel_credentials,
            insecure,
            call_credentials,
            compression,
            wait_for_ready,
            timeout,
            metadata,
        )

    @staticmethod
    def UpdateOrder(
        request,
        target,
        options=(),
        channel_credentials=None,
        call_credentials=None,
        insecure=False,
        compression=None,
        wait_for_ready=None,
        timeout=None,
        metadata=None,
    ):
        return grpc.experimental.unary_unary(
            request,
            target,
            "/orderservice.OrderService/UpdateOrder",
            orderservice__pb2.UpdateOrderRequest.SerializeToString,
            orderservice__pb2.UpdateOrderReply.FromString,
            options,
            channel_credentials,
            insecure,
            call_credentials,
            compression,
            wait_for_ready,
            timeout,
            metadata,
        )
